// function genBlob() {
//    // blob radius
//    const r = 45; 
 
//    var x = random(r, width - r),
//        y = random(r, height - r), 
//        d = random(TWO_PI),
//        speed = random(2, 3);
 
//    var blob = {
//      x: x, 
//      y: y,
//      vx: speed * cos(d),
//      vy: speed * sin(d) 
//    }
 
//    // draw blob
//    blob.draw = function() {
//      fill(125, 125);
//      ellipse(blob.x, blob.y, 2*r);
 
//      if (blob.x - r <= 0)
//        blob.vx = abs(blob.vx);
//      if (blob.x + r >= width)
//        blob.vx = -abs(blob.vx);
//      if (blob.y - r <= 0)
//        blob.vy = abs(blob.vy);
//      if (blob.y + r >= height)
//        blob.vy = -abs(blob.vy);
 
//      blob.x += blob.vx;
//      blob.y += blob.vy;
//    }
 
//    // trigger to explosion
//    blob.vanish = false;
 
//    blob.explode = function() {
//       var flakeNum = round(random(4, 6))
//       for (var i = 0; i < flakeNum; i++)
//          flakes.push(genFlake(blob.x, blob.y));
//       // for (var i = 0; i < )
//    }
 
//    blob.play = function() {
//      sound_files[blob.sid].play();
//    }
 
//    blob.click = function() {
//      var x   = mouseX - blob.x,
//          y   = mouseY - blob.y,
//          dis = sqrt(x**2 + y**2);
//      return dis <= r;
//    }
 
//    return blob;
//  }