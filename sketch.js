let blobNum;
let sources = [], sound_files = [];
let flakes = [], blobs = [], collisionMatrix = [];

function preload() {
  // load any assets (images, sounds etc.) here

  sound_files = [
    loadSound('assets/accomplished.mp3'),
    loadSound('assets/beyond-doubt-2.mp3'),
    loadSound('assets/falling-into-place.mp3'),
    loadSound('assets/for-sure.mp3'),
    loadSound('assets/just-maybe.mp3'),
    loadSound('assets/overzealous.mp3'),
    loadSound('assets/relentless.mp3'),
    loadSound('assets/undeniable.mp3'),
  ]

  for (var s of sound_files)
    s.setVolume(0.1)

  console.log(sound_files)
}

const fps = 60;

function setup() {
  createCanvas(windowWidth, windowHeight);
  // put setup code here

  blobNum = round(pow(width, 0.45));

   // number of pairs of sounds
  for (var i = 0; i < blobNum / 2; i++) {
    sources.push(i, i); // push twice of each of the pair
  }

  for (var i = 0; i < blobNum; i++) {
    var blob = genBlob();
    blob.id = i;
    var sid = sources.splice(floor(random(0, sources.length)), 1)[0];
    blob.sid = sid % sound_files.length;
    blobs.push(blob);
  }

  for (var i = 0; i < blobNum; i++) {
    var arr = [];
    for (var j = 0; j < blobNum; j++)
      arr.push(false);
    collisionMatrix.push(arr);
  }
}

var currentScene = 0;

function draw() {
  // put drawing code here
  switch (currentScene) {
    case 0:
      title();
    break;

    case 1:
      instruction();
      main();
    break;

    case 2:
      ending();
    break;
  }
}

function title() {
  fill(0);

  textSize(width / 20);
  textAlign(CENTER, CENTER);
  text("Wandering Blob", width/2, height*0.45);

  if (floor(frameCount/fps) % 2 == 0) {
    textSize(width / 35);
    text('click to start', width/2, height*0.52);
  }

  main();
}

function instruction() {
  fill(125, 125);
  const size = width/55;

  textSize(size);
  textAlign(LEFT, CENTER);

  var sentences = [
    'Every Mr.Blob has a sound',
    'You can find out its sound by clicking it',
    'If you clicked two Mr.Blobs with the same sound',
    'By the law of synchronicity',
    'They would become soul mates, hence would vanish from the scene', 
    'Leaving their happy tears on the canvas',
    'All Mr.Blobs want to find their soul mate. Please, Please help them...'
  ];
  const longest = max(sentences.map(s => s.length)),
        y = height/2 - sentences.length/2 * size - size
  for (var i = 0; i < sentences.length; i++) {
    text(sentences[i], width/2 - size * 13, y + i*size*1.5);
  }
}

function ending() {
  main();
  textAlign(CENTER, CENTER);
  textSize(width / 20);
  text('Thanks fur helping! :D', width/2, height*0.45);
}

const backgroundColor = 255;
function main() {
  background(backgroundColor, 255 * 0.4);

  for (var blob of blobs) 
    blob.draw();

  collisionDetection();

  for (var f of flakes) 
    f.draw();

  if (blobs.length == 0)
    currentScene = 2;
}

// handling mouse clicking
var lastClicked = [];

function mouseClicked() {
  if (currentScene == 0) {
    currentScene = 1;
  }
  if (currentScene == 1) {
    for (var blob of blobs) {
      if (blob.click()) {
        blob.play();
        if (lastClicked.length != 0) {
          var [osid, oid] = lastClicked;
          if (blob.sid == osid && blob.id != oid) {
            blob.explode();
            blobs.map(b => {if (b.id == oid) b.explode();})
            lastClicked = [];
            break;
          }
        }
        lastClicked = [blob.sid, blob.id];
      }
    }
  }
}

// blob radius
const r = 45; 
function genBlob() {

  var x = random(r, width - r),
      y = random(r, height - r), 
      d = random(TWO_PI),
      speed = random(2, 3);

  var blob = {
    x: x, 
    y: y,
    vx: speed * cos(d),
    vy: speed * sin(d) 
  }

  if (random() > 0.8)
    blob.color = 'red';
  else
    blob.color = color(125, 125, 125, 125);

  // draw blob
  blob.draw = function() {
    stroke(0);
    fill(blob.color);
    ellipse(blob.x, blob.y, 2*r);

    if (blob.x - r <= 0)
      blob.vx = abs(blob.vx);
    if (blob.x + r >= width)
      blob.vx = -abs(blob.vx);
    if (blob.y - r <= 0)
      blob.vy = abs(blob.vy);
    if (blob.y + r >= height)
      blob.vy = -abs(blob.vy);

    blob.x += blob.vx;
    blob.y += blob.vy;
  }

  // trigger to explosion
  blob.explode = function() {
    var flakeNum = round(random(4, 6))

    for (var i = 0; i < flakeNum; i++)
      flakes.push(genFlake(blob));

    for (var i = 0; i < blobs.length; i++) {
      if (blobs[i].id == blob.id) {
        blobs.splice(i, 1);
        break;
      }
    }
  }

  blob.play = function() {
    sound_files[blob.sid].play();
  }

  blob.click = function() {
    var x   = mouseX - blob.x,
        y   = mouseY - blob.y,
        dis = sqrt(x**2 + y**2);
    return dis <= r;
  }

  return blob;
}

// collison detection
function collisionDetection() {
  for (var i = 0; i < blobs.length; i++) {
    for (var j = i+1; j < blobs.length; j++) {
      var a = blobs[i],
          b = blobs[j];

      var dx = a.x - b.x,
          dy = a.y - b.y,
          dis = sqrt(dx*dx + dy*dy),
          minDis = 2 * r;
      
      if (dis > minDis) {
        collisionMatrix[a.id][b.id] = false;
      }
      else {
        if (!collisionMatrix[a.id][b.id]) {
          var tmpX = a.vx, tmpY = a.vy, color = a.color;
          a.vx = b.vx, a.vy = b.vy, a.color = b.color;
          b.vx = tmpX, b.vy = tmpY, b.color = color;
          collisionMatrix[a.id][b.id] = true;
        }
      }
    }
  }
}

function genFlake(blob) {
  const flakeColor = [120, 'purple', 'orange', 'yellow'];
  const size = 3;

  // random speed and directions
  var speed = random(3, 4),
      direction = random(TWO_PI);
  
  var flake = {
    x: blob.x, 
    y: blob.y,
    vx: blob.vx + speed * cos(direction),
    vy: blob.vy + speed * sin(direction),
    color: flakeColor[floor(random(flakeColor.length))]
  }

  flake.draw = function() {
    noStroke();
    fill(flake.color);
    rectMode(CENTER);
    rect(flake.x, flake.y, size, size);

    if (flake.x - size/2 <= 0 || flake.x + size/2 >= width)
      flake.vx = -flake.vx;
    if (flake.y - size/2 <= 0 || flake.y + size/2 >= height)
      flake.vy = -flake.vy;

    flake.x += flake.vx;
    flake.y += flake.vy;
  }

  return flake;
}

// // global constants
// let shorter_edge, tlx, tly, grid_length;

// function setup() {
//   createCanvas(windowWidth, windowHeight);
//   // put setup code here

//   tlx = abs(width - min(width, height) * 0.8) / 2;
//   tly = abs(height - min(width, height) * 0.8) / 2;
//   grid_length = min(width, height) / grid_num * 0.8;

//   for (var i = 0; i < grid_num; i++) {
//     map.push(new Array(grid_num));
//     cx.push(new Array(grid_num));
//     cy.push(new Array(grid_num));
//     display.push(new Array(grid_num));
//     colours.push(new Array(grid_num));
//     sounds.push(new Array(grid_num));
//   }

//   for (var i = 0; i < map.length; i++) {
//     for (var j = 0; j < map[i].length; j++) {
//       map[i][j] = 0;
//     }
//   }
//   console.log(map)

//   // generate and store circle's coord
//   for (var i = 0; i < map.length; i++) {
//     for (var j = 0; j < map[i].length; j++) {
//       var x = i * grid_length + grid_length / 2 + tlx,
//           y = j * grid_length + grid_length / 2 + tly;
//       cx[i][j] = x;
//       cy[i][j] = y;
//     }
//   }

//   // display circles or not
//   for (var i = 0; i < map.length; i++) {
//     for (var j = 0; j < map[i].length; j++) {
//       display[i][j] = true;
//     }
//   }

//   // assign circles color
//   for (var i = 0; i < map.length; i++) {
//     for (var j = 0; j < map[i].length; j++) {
//       colours[i][j] = 255;
//     }
//   }
//   console.log(sounds)
// }

// function draw() {
//   // put drawing code here
//   background(0)
//   // translate(tlx, tly)
//   frameRate(60)
//   noStroke();
//   const duration = 0.35,
//         frameNum = 60 * duration;

//   for (var i = 0; i < map.length; i++) 
//     for (var j = 0; j < map[i].length; j++) {
//       if (display[i][j]) {
//         fill(255)
        
//         if (map[i][j] > frameNum) {
//           fill(colours[i][j]);
//           ellipse(cx[i][j], cy[i][j], grid_length);
//         }
//         else {
//           fill(colours[i][j]);
//           var percentage = (map[i][j] / frameNum) ** 2;
//           var radius = lerp(0, grid_length, percentage);
//           ellipse(cx[i][j], cy[i][j], radius)
//           map[i][j] += 1;
//         }
//       }
//       else if (map[i][j] > 0) {
//         console.log(map[i][j], frameNum)
//         fill(clickedColor);
//         var percentage = 1 - (map[i][j] / frameNum) ** 2;
//         var radius = lerp(grid_length, 0, percentage);
//         ellipse(cx[i][j], cy[i][j], radius)
//         map[i][j] += 1;
//       }
//     }
//   // noLoop();
// }

// let lastClicked = [];
// const clickedColor = 'green';

// function mouseClicked() {
//   for (var i = 0; i < map.length; i++) {
//     for (var j = 0; j < map.length; j++) {
//       var x = cx[i][j];
//       var y = cy[i][j];
//       if (
//             sqrt((mouseX - x) ** 2 + (mouseY - y) ** 2) <= grid_length/2
//         &&  display[i][j]
//       ) {
//         if (lastClicked.length == 0) {
//           lastClicked.push(i, j);
//           colours[i][j] = clickedColor;
//           sound_files[sounds[i][j]].play()
//           // console.log(i, j, sounds[i][j])
//           map[i][j] = 0;
//         }
//         else {
//           sound_files[sounds[i][j]].play()

//           var li = lastClicked[0], lj = lastClicked[1];
//           // console.log(i, j, sounds[i][j])

//           if (i == li && j == lj) {
//             break;
//           }

//           if (sounds[li][lj] != sounds[i][j]) {
//             colours[li][lj] = 255;
//             // console.log(sounds[li][lj] == sounds[i][j])
//             // console.log(sounds)
//           }
//           else {
//             display[i][j] = false;
//             display[li][lj] = false;
//             colours[i][j] = clickedColor;
//             // console.log(display);
//           }

//           lastClicked = [];
//         }
//         break;
//       }
//     }
//   }
// }

