### Wandering Blob

* [Introduction](#Introduction)
* [Setup](#Setup)

### <a name="Introduction"></a>Introduction
Each wandering blob has its own voice, and it's trying the one blob that shares the same voice with him. Click through them and try to help them find their match!

### <a name="Setup"></a>Setup

I used [VS Code](https://code.visualstudio.com/) with the [live server extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) to develop the artwork. A typical setup progress would be:
1. Install [VS Code](https://code.visualstudio.com/).
2. Within VS Code, install the [live server extension](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer).
3. Clone the repo to your local machine.
4. Open the repo with VS Code.
5. Start live server within VS Code for the project. If you don't know how, you can check out the [Shortcuts to Start/Stop Sever. session](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer#shortcuts-to-startstop-server) in the live server extension page.
6. VSCode should automatically open the browser and load the artwork into it. 
7. Enjoy the match ;)

The above setup guide is just an example. Theoratically, any local web server would be able to run the interactive artwork, hence options other than VSCode should work as well. 

